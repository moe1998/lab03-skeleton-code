package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";

		// lifecycle counts
		//TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
		//TODO:  increment the variables' values when their corresponding lifecycle methods get called.

		private int createCounter = 0;
		private int startCounter = 0;
		private int resumeCounter = 0;
		private int pauseCounter = 0;
		private int stopCounter = 0;
		private int restartCounter = 0;
		private int destroyCounter = 0;
		TextView createTextView;
		TextView startTextView;
		TextView resumeTextView;
		TextView pauseTexrView;
		TextView stopTextView;
		TextView restartTextView;
		TextView destroyTextView;

		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_one);

			//Log cat print out
			Log.i(TAG, "onCreate called");

            createTextView = (TextView) findViewById(R.id.create);
            startTextView = (TextView) findViewById(R.id.start);
            resumeTextView = (TextView) findViewById(R.id.resume);
            pauseTexrView = (TextView) findViewById(R.id.pause);
            stopTextView = (TextView) findViewById(R.id.stop);
            restartTextView = (TextView) findViewById(R.id.restart);
            destroyTextView = (TextView) findViewById(R.id.destroy);

            String temp = getResources().getString(R.string.onCreate) + ++createCounter;
            createTextView.setText(temp);
			//TODO: update the appropriate count variable & update the view
		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_one, menu);
			return true;
		}
		
		// lifecycle callback overrides
		
		@Override
		public void onStart(){
			super.onStart();

			//Log cat print out
			Log.i(TAG, "onStart called");

			//TODO:  update the appropriate count variable & update the view
            String temp = getResources().getString(R.string.onStart) + ++startCounter;
            startTextView.setText(temp);
		}

	    // TODO: implement 5 missing lifecycle callback methods

        @Override
        public void onResume(){
		    super.onResume();

		    Log.i(TAG, "onResume called");
		    String temp = getResources().getString(R.string.onResume) + ++resumeCounter;
		    resumeTextView.setText(temp);
        }

        @Override
        public void onPause(){
            super.onPause();

            Log.i(TAG,"onPause called");
            String temp = getResources().getString(R.string.onPause) + ++pauseCounter;
            pauseTexrView.setText(temp);
        }

        @Override
        public void onStop(){
		    super.onStop();

		    Log.i(TAG, "onStop called");
		    String temp = getResources().getString(R.string.onStop) + ++stopCounter;
		    stopTextView.setText(temp);
        }

        @Override
        public void onRestart(){
		    super.onRestart();

		    Log.i(TAG, "onRestart called");
		    String temp = getResources().getString(R.string.onRestart) + ++restartCounter;
		    restartTextView.setText(temp);
        }

        @Override
        public void onDestroy(){
		    super.onDestroy();

		    Log.i(TAG, "onDestroy called");
		    String temp = getResources().getString(R.string.onDestroy) + ++destroyCounter;
		    destroyTextView.setText(temp);
        }

	    // Note:  if you want to use a resource as a string you must do the following
	    //  getResources().getString(R.string.stringname)   returns a String.

		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			//TODO:  save state information with a collection of key-value pairs & save all  count variables
            super.onSaveInstanceState(savedInstanceState);
			savedInstanceState.putInt("create", createCounter);
			savedInstanceState.putInt("start", startCounter);
			savedInstanceState.putInt("resume", resumeCounter);
			savedInstanceState.putInt("pause", pauseCounter);
			savedInstanceState.putInt("stop", stopCounter);
			savedInstanceState.putInt("restart", restartCounter);
			savedInstanceState.putInt("destroy", destroyCounter);
		}


		public void launchActivityTwo(View view) {
			startActivity(new Intent(this, ActivityTwo.class));
		}
		

}
